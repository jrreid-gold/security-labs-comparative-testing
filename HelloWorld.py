# Unproblematic code
print("Hello world")

# SAST should find a problem with this code
try:
    do_something()
except:
    handle_exception()

# Secret Detection should find a problem with this code
GL_Token = 'glpat-hC5G3PrMaZ7UkVZhxhnx'
